from requests import get, post
from json import loads
from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn

app = FastAPI()

@app.get("/")
async def root():
    return {"message" : "Calculadora API"}

class Resp(BaseModel):
    valor1: int
    valor2: int
    operacao: int

@app.post("/resultado")
async def resultado_calculadora(valor1, valor2, operacao):

    """
    Esta é uma api de uma calculadora simles.
    Digite os valores de valor1 e valor2 como números inteiros,
    e a operacao desejada com '1' para soma, '2' para subtração,
    '3' para multiplicação, e '4' para divisão.
    """
    if operacao == "1":
        resultado = valor1 + valor2
        return {"Resultado da soma" : resultado}
    if operacao == "2":
        resultado = valor1 - valor2
        return {"Resultado da subtração" : resultado}
    if operacao == "3":
        resultado = valor1 * valor2
        return {"Resultado da multiplicação" : resultado}
    if operacao == "4":
        if valor2 != 0:
            resultado = valor1 / valor2
            return {"Resultado da divisão" : resultado}
        else:
            return{"Erro na divisão por 0"}
    else:
        return{"Ops! Operação matemática não encontrada"}


if __name__ == '__main__':
    uvicorn.run("exercicio06:app",
                port=8000,
                reload=True,
                )
