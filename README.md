# API Python
Arquivos para correção do módulo 06 do curso "Python Pro" criando uma API em Python para criação de uma calculadora que deverá receber uma requisição através de um méthodo POST no formato json e realizar a operação matemática desejada


## Instalação
git clone https://gitlab.com/gbiotto/api-python.git

## Como utilizar
python exercicio06.py
Neste arquivo o usuário deverá passar por parâmetro dois valores de [valor1] e [valor2] que será utilizado para realizar a operação matemática [operacao] que também devera ser passado por parâmetro com os seguintes valores:
1 - Adição | 
2 - Subtração | 
3 - Multiplicação | 
4 - Divisão

## Comandos relevantes
pip install -U fastapi[all]
